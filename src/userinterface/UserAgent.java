package userinterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Map.Entry;

import lib.DatiUtenti;
import lib.UtentiInterface;
import lib.agentInterface;




public class UserAgent implements agentInterface {

	/**
	 * 
	 */
	
	private static int fail = 0;
	@SuppressWarnings("unused")
	private int create;

	protected UserAgent() throws RemoteException {
		super();
	}

	/**
	 * @param args
	 * @throws NotBoundException 
	 * @throws RemoteException 
	 * @throws MalformedURLException 
	 */
	static DatiUtenti user1,user2 ;
	
	UserAgent agent;
	static String entrata ;
	static String uscita ;
	static int PORT = 1333;
	static Thread reciver;
	static GossipTCP proxy;
	static UserAgent obj ;
	static agentInterface stub;
	static UtentiInterface utente;
	static InetAddress ip;
	static String ip1, ipProx;
	public static void main(String[] args) throws NotBoundException, IOException {
		String input = null;
		String pass= null;
		String nick = null;
		UtentiInterface ute;
		Registry rmiUtenti ;
		user1= new DatiUtenti();
		int esci=0;
		
		
		Enumeration<InetAddress> addresses=NetworkInterface.getByName("eth1").getInetAddresses();
		InetAddress nextAddress=null;
		while(addresses.hasMoreElements()){
			nextAddress=addresses.nextElement();
			if(!(nextAddress instanceof Inet4Address)) nextAddress=null;
		}
//		se non ho trovato la scheda di rete mi accontento del metodo statico di InetAddress getLocalHost
		if(nextAddress==null) {
			nextAddress=InetAddress.getLocalHost();
		}
		
		String localIP = nextAddress.getHostAddress();
//		setto rmi del registry
		try {
			
			 System.setProperty("java.rmi.server.hostname", localIP);
			 
//			 leggo ip registry dal file
			 BufferedReader ausi = new BufferedReader (new FileReader("IpRegistry"));
			 String lettura = ausi.readLine();
			 ausi.close();
			 rmiUtenti = LocateRegistry.getRegistry(lettura, 11112);
			 
			 if(rmiUtenti == null ) return;
			 try{
			ute =  (UtentiInterface) rmiUtenti.lookup("Intutenti");
			utente=ute;
			 }
			 catch(NullPointerException e) {
				 e.printStackTrace();
			 }
			 catch  (NotBoundException e1){
				 e1.printStackTrace();
				 
				 
			 }
			 catch(AccessException e2){
				 e2.printStackTrace();
			 }
		
			
			
//			setto rmi dell'agent
			obj= new UserAgent();
		
			stub =  (agentInterface) UnicastRemoteObject.exportObject(obj, 0);
			
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		DatagramSocket socket=null;
		BufferedReader stdIn =
                new BufferedReader(new InputStreamReader(System.in));
//		lista dei comandi
		System.out.println("lista dei comandi: \n "
				+ "1) crea \n "
				+ "2)login \n "
				+ "3) logout \n "
				+ "4) addIngresso \n "
				+ "5) addUscita \n "
				+ "6) chat \n "
				+ "7) quit \n"
				+ "8) cancel \n");
		while(esci==0 	){
			System.out.printf("comando? \n");
			try {
				input= stdIn.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			switch (input){
				case "chat":{
					if(fail==1){
						System.out.printf("inserire nome di chi vuoi contattare...\n");
						if((input= stdIn.readLine())!=null){
							String	dest = input;
							if(user1.contattiUscita.containsKey(dest)==false){
								System.out.printf("non hai nessun contatto in uscita chiamato %s \n",dest);
								break;
							}
							String ipDest = utente.GetIp(user1.nome,dest);
							System.out.printf("digita il mex da spedire.... \n");
							if((input= stdIn.readLine())!=null){
//						user online uso UDP
								if((user1.contattiUscita.get(dest))==1){
									GossipUDP.sendMex(InetAddress.getByName(ipDest),input);
								}
//						user offline uso TCP verso il proxy di indiritto ipDest
								else{
									
									if(input!=null) {
										proxy =new  GossipTCP(ipDest,user1.nome);
										proxy.Send(user1.nome,dest,input);
										proxy=null;
									}
								}
							}
						}
						break;
					}
					else{
						System.out.printf("loggarsi prima....\n");
						break;
					}
					
				}
				case "crea":{
					if(fail!=1){
						System.out.printf("nick \n");
						try {
							if((input= stdIn.readLine())!=null){
								nick = input;
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						System.out.printf("pass \n");
						try {
							if((input= stdIn.readLine())!=null){
								pass = input;
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						
						user1 =  new DatiUtenti(nick ,pass ,localIP);
					
						try {
							if((utente.Create(nick, pass, localIP, stub)==1)){
								entrata = "./entrata"+nick;
								uscita = "./uscita"+nick;
								
								// thread UDP che riceve e invia messaggi a utenti online
								socket=new DatagramSocket(PORT);
								reciver=new Thread(new GossipUDP(socket,nick));
								reciver.start();
								
								fail=1;
							}
						} catch (RemoteException e) {
							e.printStackTrace();
						} catch (MalformedURLException e) {
							e.printStackTrace();
						}
						break;
					}
					else{
						System.out.printf("gia loggato\n");
						break;
					}
				}
				case "addIngresso":{
					if(fail==1){
						System.out.printf("nome da aggiungere \n");
						try {
							if((input= stdIn.readLine())!=null){
								nick = input;
								if(utente.AddIngresso(user1.nome,input )==1){
								user1.AddIngressoLoc(nick,utente.GetStato(user1.nome,nick));
								 File file = new File(entrata);
								 FileWriter   w=new FileWriter(file,true);
							    w.write(input);
							    w.flush();
							    w.close();
								}
								else{
									System.out.printf("ritenta...\n");
								}
								break;
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					else{
						System.out.printf("loggarsi\n");
						break;
					}
				}
				case "addUscita":{
					if(fail!=1){
						System.out.printf("loggarsi\n");
						break;
					}
					System.out.printf("nome da aggiungere \n");
					try {
						if((input= stdIn.readLine())!=null){
							nick = input;
							if(utente.AddUscita(user1.nome,input )==1){
								user1.contattiUscita.put(nick,(Integer) utente.GetStato(user1.nome,nick));
							if(user1.contattiUscita.containsKey(nick)==true){
							}
							File file = new File(uscita);
							FileWriter outputStream = new FileWriter ( (file),true);
							outputStream.write(input);
							outputStream.flush();
							outputStream.close();
							}
							else{
								System.out.printf("non hai il contatto nella tua lista in ingresso o non sei nella sua lista di contatti in ingresso\n");
							}
							break;
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				case "login":{
					if(fail!=1){
						System.out.printf("nick? \n");
						try {
							if((input=stdIn.readLine())!=null){
								nick= input;
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						System.out.printf("pass \n");
						try {
							if((input = stdIn.readLine())!=null){
								pass = input;
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						utente.Login(nick,pass,stub,localIP);
						
						if(fail==1){
						
							user1 =  new DatiUtenti(nick ,pass ,localIP);
							try {
								obj.LogIn(nick, pass);
							} catch (RemoteException | MalformedURLException e) {
								e.printStackTrace();
							}
							// thread UDP che riceve e invia messaggi a utenti online
							
							socket=new DatagramSocket(PORT);
							
							reciver=new Thread(new GossipUDP(socket,nick));
							reciver.start();
					
							proxy =new GossipTCP(ipProx,user1.nome);
							proxy.reciver(user1.nome);
							proxy=null;
							break;
						}
						else {
							System.out.printf("login fallito!!! \n");
							break;
						}
					}
					else{
						System.out.printf("gia loggato\n");
						break;
					}
				}
				case "logout":{
					if(fail!=1){
						System.out.printf("loggarsi\n");
						break;
					}
					try {
						if(utente.LogOut(user1.nome)==1){
							socket.close();
							break;
						}
						else{
							System.out.println("logout fallito ritenta");
							break;
						}
					} catch (RemoteException | MalformedURLException e) {
						e.printStackTrace();
					}
					
				}

				case "quit":{
					if(fail==0){
					esci = 1;
					try {
						reciver.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					stdIn.close();
//					
					UnicastRemoteObject.unexportObject(obj, true);
					
					break;
					}
					else{
						System.out.println("effettuare prima il logout");
						break;
					}
				}
				
				case "cancel":{
					System.out.printf("digita il contatto da eliminare\n");
					try {
						if((input=stdIn.readLine())!=null){
							nick= input;
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					if(user1.contattiIngresso.containsKey(nick)==true){
						utente.cancel(user1.nome,nick);
						obj.cancelUser(nick);
					}
					else{
						System.out.printf("non hai nei contati l'utente %s",nick);
					}
					break;
				}
			}
		}
		return;
	}
		
		

	
//rimuovo l'utente dalle liste a solo dopo lo romuovo anche dai file facendo prima una copia dei file in caso di errori
	public void cancelUser(String nick) throws RemoteException{
	
		if(user1.contattiIngresso.containsKey(nick)==true){
			user1.contattiIngresso.remove(nick);
			if(user1.contattiUscita.containsKey(nick)==true){
				user1.contattiUscita.remove(nick);
			}
		try {
			obj.ReloadFile();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.printf("errore nel cancellare l'utente \n");
			return;
		}
//		ho gia la copia dei file e aggiornati , cancello i vecchi file e rinomino i nuovi
		File file = new File("entrata"+user1.nome);
		File file2 = new File("uscita"+user1.nome);
		file.delete();
		file2.delete();
		file = new File("entrata"+user1.nome);
		file2 = new File("uscita"+user1.nome);
		File fileNew = new File("entrataTemp");
		File file2New = new File("uscitaTemp");
		 
		
		 fileNew.renameTo(file);
		 file2New.renameTo(file2);
		 
			return;
		}
		
	}




//segnalo che un utente si � connesso
	@Override
	public void UserOn(String name, String ip) throws RemoteException {
		System.out.printf("utente connesso %s \n", name);
		user1.contattiUscita.replace(name, 1);
		user1.contattiIngresso.replace(name,1);
		
	}
// segnalo che un utente si � sconnesso
	@Override
	public void UserOff(String name, String ip) throws RemoteException {
		System.out.printf("utente %s sconnesso \n", name);
		user1.contattiUscita.replace(name, 0);
		user1.contattiIngresso.replace(name,0);
	
	}

	@Override
	public void IdProxy(String ip) throws RemoteException {
		user1.setIpProxy(ip);
		ipProx=ip;
	}

	@Override
	public void LogIn(String name, String pass) throws RemoteException, MalformedURLException, NotBoundException {
		entrata = "./entrata"+name;
		uscita = "./uscita"+name;
		File file = new File(entrata);
		File file2 = new File(uscita);
		if(fail==1){
		 //carico gli utenti in listaEntrata
		//carico gli utenti in listaUscita
	        try {
	        	agent= new UserAgent();
				agent.CaricaIn(file, user1.nome);
				agent.CaricaOut(file2, user1.nome);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
			
	}


//dopo la procedura di rimuovere un utente dai contatti creo dei temp in cui salvare la lista di utente aggioranta
	private void ReloadFile() throws IOException{
		File file = new File("entrataTemp");
		File file2 = new File("uscitaTemp");
		FileWriter w;
		
		
			w=new FileWriter(file,true);
			Iterator<Entry<String, Integer>> iteretorOUT = user1.contattiIngresso.entrySet().iterator();
			while(iteretorOUT.hasNext()){
				Entry<String, Integer> entry= iteretorOUT.next();
				w.write(entry.getKey()+"\n");
				
			}
			w.flush();
			w.close();
			
			w=new FileWriter(file2,true);
			iteretorOUT = user1.contattiUscita.entrySet().iterator();
			while(iteretorOUT.hasNext()){
				Entry<String, Integer> entry= iteretorOUT.next();
				w.write(entry.getKey()+"\n");
				
			}
			w.flush();
			w.close();
			
		
		
	}



	@Override
	public int FailLog(int x) throws RemoteException {
		return fail = x;
	}
	public int Creat(int x) throws RemoteException {
		return create = x;
	}
	
	private void CaricaIn(File in,  String name) throws IOException{
		 Scanner s = null;
		 String ute1;
		 if(in.exists()){
			 try {//lo scanner mi permette di leggere fino ad "acapo" le righe di un file è tipo un iteretor quindi ha il metodo hasnext
				 s = new Scanner(new BufferedReader(new FileReader(in)));
	         
				 while (s.hasNext()) {
	        	  
					 ute1 = s.next();
					 DatiUtenti user = new DatiUtenti();
					 user.SetStato((int) utente.GetStato(name, ute1));
					 user.nome=ute1;
					 user1.AddIngressoLoc(ute1,  utente.GetStato(name, ute1));
				 }
			 } finally {
				 if (s != null) {
					 s.close();
				 }
	      }
		 }
	}
	private void CaricaOut(File out,  String name) throws IOException{
		 Scanner s = null;
		 String ute1;
		 if(out.exists()){
			 try {//lo scanner mi permette di leggere fino ad "acapo" le righe di un file è tipo un iteretor quindi ha il metodo hasnext
	          	s = new Scanner(new BufferedReader(new FileReader(out)));
	          	while (s.hasNext()) {
	        	  
	        	  	ute1 = s.next();
	        	  	DatiUtenti user = new DatiUtenti();
	        	  	user1.contattiUscita.put(ute1,utente.GetStato(name,ute1));
	        	  	user.SetMyIp(utente.GetIp(name, ute1));
	          	}
	      	} finally {
	          	if (s != null) {
	        	  	s.close();
	          }
	      	}
		 }
	}	 
}
