package userinterface;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class GossipTCP {
	private Socket socket;
	DataOutputStream out;
	DataInputStream in;
	String nome;
	
	//porta fissa del proxy
	private static final int port=1115;
//	metodo costruttore, creao la socket e i due stream di entrata e uscita
	public GossipTCP(String ip,String nome) throws UnknownHostException, IOException{
		socket= new Socket(InetAddress.getByName(ip),port);
		out= new DataOutputStream(socket.getOutputStream());
		in= new DataInputStream(socket.getInputStream());
		this.nome=nome;
		
	}
	public boolean Send(String nome, String dest, String input) {
		
		try {
			out.writeUTF(nome);
			out.writeUTF("SND");
			out.writeUTF(dest);
			out.writeUTF(input);
			close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			close();
			return false;
		}
		
	}
	
	public void close(){
		try {
			out.close();
			in.close();
			socket.close();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	public void reciver(String nome) throws IOException {
		String mex;
		boolean ok=true;
		out.writeUTF(nome);
		out.writeUTF("RCV");
		while(ok){
			mex=in.readUTF();
			if(mex.compareTo("EOF")!=0){
				System.out.printf("%s \n",mex+"\n");
				System.out.flush();
			}
			else
				ok=false;
		}
		System.out.printf("non ci sono messaggi \n");
		close();
		System.out.println("Fine ricezione messaggi");
		return;
	}

}
