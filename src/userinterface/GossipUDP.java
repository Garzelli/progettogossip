package userinterface;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class GossipUDP implements Runnable {
//	socket UDP gestita da questo oggetto
	static DatagramSocket socket;
	static int PORT=1333;
	static boolean ok = true;
	static String nick;
//	pacchetto per l'invio dati UDP
	static byte[] data=new byte[20];
	static DatagramPacket pkt=new DatagramPacket(data, 0);
//	dichiaro lo stream di uscita
	static ByteArrayOutputStream output=new ByteArrayOutputStream();
	static DataOutputStream terminal=new DataOutputStream(output);
//stream entrata
	ByteArrayInputStream arrayIN;
	DataInputStream IN;
	String mex;
	
	
	public GossipUDP(DatagramSocket socket2, String nick2) {
		nick=nick2;
		socket=socket2;
		pkt.setPort(PORT);
	}



	@Override
	public void run() {
//		pacchetto di lettura
		byte[] buf=new byte[200];
		DatagramPacket rcv=new DatagramPacket(buf, buf.length);
		
//		lettura
		while(socket.isClosed()!=true){
			try {
				socket.receive(rcv);
			}
			catch(SocketException e1){
//				socket chiusa quindi utente offline
				
				try {
					terminal.close();
					output.close();
					rcv=null;
					return;
				} catch (IOException e) {
					return;
				}
				
				
			}
			catch (IOException e) {
				e.printStackTrace();
				return;
			}
//			estraggo dati dal pacchetto
			arrayIN=new ByteArrayInputStream(rcv.getData(), 0, rcv.getLength());
			IN=new DataInputStream(arrayIN);
			try {
				mex=IN.readUTF();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
//			formatto il mex e stampo
			String[] splitmex=mex.split(":", 2);
			System.out.printf("%s scrive : %s \n",splitmex[0],splitmex[1]);
			
		}
		
	}
static void sendMex(InetAddress ip,String mess) {
	pkt.setAddress(ip);
	try {
		terminal.writeUTF(nick+":"+mess);
		data=output.toByteArray();
		pkt.setData(data,0,data.length);
		socket.send(pkt);
		output.reset();
	} catch (IOException e) {
		e.printStackTrace();
		return;
	}
	return;
}



}
